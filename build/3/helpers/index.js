"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.reducer = exports.isNumeric = exports.prepareDefaultMatrixData = exports.loadData = exports.saveData = void 0;
var fs_1 = require("fs");
var path_1 = require("path");
var saveData = function (path, data) {
    return fs_1.writeFileSync(path_1.resolve(path), data);
};
exports.saveData = saveData;
var loadData = function (path, options) {
    return fs_1.readFileSync(path_1.resolve(path), options);
};
exports.loadData = loadData;
var prepareDefaultMatrixData = function (path, data) {
    var fullPath = path_1.resolve(path);
    !fs_1.existsSync(fullPath) && saveData(fullPath, data);
};
exports.prepareDefaultMatrixData = prepareDefaultMatrixData;
var isNumeric = function (num) {
    return !isNaN(num);
};
exports.isNumeric = isNumeric;
var reducer = function (accumulator, currentValue) {
    return accumulator + currentValue;
};
exports.reducer = reducer;
