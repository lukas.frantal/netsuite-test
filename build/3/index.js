"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.reducer = exports.isNumeric = exports.prepareDefaultMatrixData = exports.loadData = exports.saveData = exports.sumMatrix = exports.setMatrixValue = void 0;
var helpers_1 = require("./helpers");
Object.defineProperty(exports, "saveData", { enumerable: true, get: function () { return helpers_1.saveData; } });
Object.defineProperty(exports, "loadData", { enumerable: true, get: function () { return helpers_1.loadData; } });
Object.defineProperty(exports, "prepareDefaultMatrixData", { enumerable: true, get: function () { return helpers_1.prepareDefaultMatrixData; } });
Object.defineProperty(exports, "isNumeric", { enumerable: true, get: function () { return helpers_1.isNumeric; } });
Object.defineProperty(exports, "reducer", { enumerable: true, get: function () { return helpers_1.reducer; } });
var setMatrixValue = function (positionRow, positionCol, value, path) {
    if (typeof positionRow !== 'number') {
        throw new Error('first input in the function shoud be number');
    }
    if (typeof positionCol !== 'number') {
        throw new Error('second input in the function shoud be number');
    }
    if (typeof value !== 'number') {
        throw new Error('third input in the function shoud be number');
    }
    if (typeof path !== 'string') {
        throw new Error('fourth input in the function shoud be string');
    }
    var loadedData = helpers_1.loadData(path, 'utf8');
    if (!loadedData && typeof loadedData !== 'string') {
        throw new Error('Initial matrix data are not valid should be string in folder data/matrix.txt');
    }
    var array = loadedData.split('\n');
    var lengthRow = array.length;
    if (positionRow > lengthRow) {
        throw new Error("Initial matrix data has " + lengthRow + " rows and your " + positionRow + " position row doesn't exist in matrix");
    }
    var lengColumn = array[0]
        .replace('(', '')
        .replace(')', '')
        .split(' ')
        .filter(function (item) { return item !== ''; }).length;
    if (positionCol > lengColumn) {
        throw new Error("Initial matrix data has " + lengColumn + " columns and your " + positionCol + " position colmn doesn't exist in matrix");
    }
    var result = array
        .map(function (row, index) {
        var rowData = row.replace('(', '').replace(')', '').split(' ');
        var filteredRow = rowData.filter(function (item) { return item !== ''; });
        if (index === positionRow) {
            filteredRow[positionCol] = String(value);
        }
        return "( " + filteredRow.join(' ') + " )";
    })
        .join('\n');
    return result;
};
exports.setMatrixValue = setMatrixValue;
var sumMatrix = function (positionRow, positionCol, path) {
    if (typeof positionRow !== 'number') {
        throw new Error('first input in the function shoud be number');
    }
    if (typeof positionCol !== 'number') {
        throw new Error('second input in the function shoud be number');
    }
    if (typeof path !== 'string') {
        throw new Error('fourth input in the function shoud be string');
    }
    var loadedData = helpers_1.loadData(path, 'utf8');
    if (!loadedData) {
        throw new Error('Initial Data are empty');
    }
    var array = loadedData.split('\n');
    var lengthRow = array.length;
    if (positionRow > lengthRow) {
        throw new Error("Initial matrix data has " + lengthRow + " rows and your " + positionRow + " position row doesn't exist in matrix");
    }
    var lengColumn = array[0]
        .replace('(', '')
        .replace(')', '')
        .split(' ')
        .filter(function (item) { return item !== ''; }).length;
    if (positionCol > lengColumn) {
        throw new Error("Initial matrix data has " + lengColumn + " columns and your " + positionCol + " position colmn doesn't exist in matrix");
    }
    var result = 0;
    array.map(function (row, index) {
        var rowData = row.replace('(', '').replace(')', '').split(' ');
        var filteredRow = rowData.filter(function (item) { return item !== ''; });
        if (index <= positionRow) {
            result += Number(filteredRow.slice(0, positionCol + 1).reduce(function (acc, curr) {
                if (helpers_1.isNumeric(acc) && helpers_1.isNumeric(curr)) {
                    return helpers_1.reducer(Number(acc), Number(curr));
                }
                else {
                    throw new Error("Matrix can't be sum because input matrix has value which is not number");
                }
            }));
        }
    });
    return result;
};
exports.sumMatrix = sumMatrix;
