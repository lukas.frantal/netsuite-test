"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.sortZerosAtEnd = void 0;
var valueIsNotZero = function (element) { return element !== 0; };
var valueIsZero = function (element) { return element === 0; };
var sortZerosAtEnd = function (inputArray) {
    if (inputArray && !inputArray.some(function (item) { return typeof item !== 'number'; })) {
        return inputArray
            .filter(valueIsNotZero)
            .concat(inputArray.filter(valueIsZero));
    }
    throw new Error('Your input should array with numbers');
};
exports.sortZerosAtEnd = sortZerosAtEnd;
