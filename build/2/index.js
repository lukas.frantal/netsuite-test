"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.palindrome = void 0;
var palindrome = function (inputString) {
    if (typeof inputString === 'string') {
        var re = /[\W_]/g;
        var lowRegStr = inputString.replace(re, '');
        var reverseStr = lowRegStr.split('').reverse().join('');
        return reverseStr === lowRegStr;
    }
    throw new Error('Your input should string');
};
exports.palindrome = palindrome;
