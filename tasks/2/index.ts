export const palindrome = (inputString: string): boolean | Error => {
	if (typeof inputString === 'string') {
		const re = /[\W_]/g
		const lowRegStr = inputString.replace(re, '')
		const reverseStr = lowRegStr.split('').reverse().join('')
		return reverseStr === lowRegStr
	}
	throw new Error('Your input should string')
}
