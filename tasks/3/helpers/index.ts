import { existsSync, readFileSync, writeFileSync } from 'fs'
import { resolve } from 'path'

const saveData = (path: string, data: string) =>
	writeFileSync(resolve(path), data)

const loadData = (path: string, options?: string | any): string | any =>
	readFileSync(resolve(path), options)

const prepareDefaultMatrixData = (path: string, data: string) => {
	const fullPath = resolve(path)
	!existsSync(fullPath) && saveData(fullPath, data)
}

const isNumeric = (num: any): boolean => {
	return !isNaN(num)
}

const reducer = (accumulator: number, currentValue: number): any => {
	return accumulator + currentValue
}

export { saveData, loadData, prepareDefaultMatrixData, isNumeric, reducer }
