import {
	saveData,
	loadData,
	prepareDefaultMatrixData,
	isNumeric,
	reducer,
} from './helpers'

const setMatrixValue = (
	positionRow: number,
	positionCol: number,
	value: number,
	path: string
): String | Error => {
	if (typeof positionRow !== 'number') {
		throw new Error('first input in the function shoud be number')
	}
	if (typeof positionCol !== 'number') {
		throw new Error('second input in the function shoud be number')
	}
	if (typeof value !== 'number') {
		throw new Error('third input in the function shoud be number')
	}
	if (typeof path !== 'string') {
		throw new Error('fourth input in the function shoud be string')
	}
	const loadedData = loadData(path, 'utf8')
	if (!loadedData && typeof loadedData !== 'string') {
		throw new Error(
			'Initial matrix data are not valid should be string in folder data/matrix.txt'
		)
	}

	const array = loadedData.split('\n')

	const lengthRow = array.length
	if (positionRow > lengthRow) {
		throw new Error(
			`Initial matrix data has ${lengthRow} rows and your ${positionRow} position row doesn't exist in matrix`
		)
	}
	const lengColumn = array[0]
		.replace('(', '')
		.replace(')', '')
		.split(' ')
		.filter((item: string) => item !== '').length
	if (positionCol > lengColumn) {
		throw new Error(
			`Initial matrix data has ${lengColumn} columns and your ${positionCol} position colmn doesn't exist in matrix`
		)
	}

	const result = array
		.map((row: string, index: number) => {
			const rowData = row.replace('(', '').replace(')', '').split(' ')
			const filteredRow = rowData.filter(item => item !== '')
			if (index === positionRow) {
				filteredRow[positionCol] = String(value)
			}
			return `( ${filteredRow.join(' ')} )`
		})
		.join('\n')
	return result
}

const sumMatrix = (
	positionRow: number,
	positionCol: number,
	path: string
): number | Error => {
	if (typeof positionRow !== 'number') {
		throw new Error('first input in the function shoud be number')
	}
	if (typeof positionCol !== 'number') {
		throw new Error('second input in the function shoud be number')
	}
	if (typeof path !== 'string') {
		throw new Error('fourth input in the function shoud be string')
	}
	const loadedData = loadData(path, 'utf8')
	if (!loadedData) {
		throw new Error('Initial Data are empty')
	}

	const array = loadedData.split('\n')
	const lengthRow = array.length
	if (positionRow > lengthRow) {
		throw new Error(
			`Initial matrix data has ${lengthRow} rows and your ${positionRow} position row doesn't exist in matrix`
		)
	}
	const lengColumn = array[0]
		.replace('(', '')
		.replace(')', '')
		.split(' ')
		.filter((item: string) => item !== '').length
	if (positionCol > lengColumn) {
		throw new Error(
			`Initial matrix data has ${lengColumn} columns and your ${positionCol} position colmn doesn't exist in matrix`
		)
	}
	let result = 0
	array.map((row: string, index: number) => {
		const rowData = row.replace('(', '').replace(')', '').split(' ')
		const filteredRow = rowData.filter(item => item !== '')
		if (index <= positionRow) {
			result += Number(
				filteredRow.slice(0, positionCol + 1).reduce((acc: any, curr: any) => {
					if (isNumeric(acc) && isNumeric(curr)) {
						return reducer(Number(acc), Number(curr))
					} else {
						throw new Error(
							"Matrix can't be sum because input matrix has value which is not number"
						)
					}
				})
			)
		}
	})
	return result
}

export {
	setMatrixValue,
	sumMatrix,
	saveData,
	loadData,
	prepareDefaultMatrixData,
	isNumeric,
	reducer,
}
