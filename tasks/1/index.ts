const valueIsNotZero = (element: number): boolean => element !== 0
const valueIsZero = (element: number): boolean => element === 0
export const sortZerosAtEnd = (inputArray: number[]): number[] | Error => {
	if (inputArray && !inputArray.some(item => typeof item !== 'number')) {
		return inputArray
			.filter(valueIsNotZero)
			.concat(inputArray.filter(valueIsZero))
	}
	throw new Error('Your input should array with numbers')
}
