const { sortZerosAtEnd } = require('../build/1')
const { palindrome } = require('../build/2')
const {
	prepareDefaultMatrixData,
	setMatrixValue,
	sumMatrix,
} = require('../build/3')

// task 1
const task1Input = [1, 3, 0, 8, 12, 0, 4, 0, 7]
const task1Result = sortZerosAtEnd(task1Input)
console.log(
	`Task1\ninput: ${task1Input}\nresult: ${task1Result}\nit is O(n) complexity\n----------------`
)
// task 2
const task2Input = 'Deed'
const task2Result = palindrome(task2Input)
console.log(
	`Task2\ninput: ${task2Input}\nresult: ${task2Result}\nit is O(n) complexity\n----------------`
)
// task 3
const path = './data/matrix.txt'
const data = `( 0  3  0  0 )\n( 1  0  0  0 )\n( 0  0  4  -1)\n( 0  1  0  0 )`
prepareDefaultMatrixData(path, data)
console.log(`Task3\n- set(0, 1, 5)\nresult:\n${setMatrixValue(0, 1, 5, path)}`)
console.log(`- sum(2,2):\nresult:\nA = ${sumMatrix(2, 2, path)}`)
