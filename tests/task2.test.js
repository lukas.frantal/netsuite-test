const { palindrome } = require('../build/2')

describe('Task2', () => {
	test("Testing correct input 'Deed'", () => {
		expect(palindrome('Deed')).toMatchSnapshot()
	})
	test("Testing correct input 'deed'", () => {
		expect(palindrome('deed')).toMatchSnapshot()
	})
	test("Testing correct input '12121'", () => {
		expect(palindrome('12121')).toMatchSnapshot()
	})
	test('Testing wrong input undefined', () => {
		expect(() => palindrome(undefined)).toThrowErrorMatchingSnapshot()
	})
	test('Testing wrong input undefined', () => {
		expect(() => palindrome(1)).toThrowErrorMatchingSnapshot()
	})
	test('Testing wrong input undefined', () => {
		expect(() => palindrome(true)).toThrowErrorMatchingSnapshot()
	})
})
