const { sortZerosAtEnd } = require('../build/1')

describe('Task1', () => {
	test('Testing correct input [1, 3, 0, 8, 12, 0, 4, 0, 7]', () => {
		expect(sortZerosAtEnd([1, 3, 0, 8, 12, 0, 4, 0, 7])).toMatchSnapshot()
	})
	test('Testing wrong input undefined', () => {
		expect(() => sortZerosAtEnd(undefined)).toThrowErrorMatchingSnapshot()
	})
	test("Testing wrong input ['1', 3, 0]", () => {
		expect(() => sortZerosAtEnd(['1', 3, 0])).toThrowErrorMatchingSnapshot()
	})
	test('Testing wrong input [true]', () => {
		expect(() => sortZerosAtEnd([true])).toThrowErrorMatchingSnapshot()
	})
})
