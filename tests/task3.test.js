const { setMatrixValue, sumMatrix } = require('../build/3')

describe('Task3', () => {
	const path = './data/matrix.txt'

	describe('setMatrixValue()', () => {
		test(`Testing correct input setMatrixValue(0, 1, 5, ${path})`, () => {
			expect(setMatrixValue(0, 1, 5, path)).toMatchSnapshot()
		})
		test(`Testing wrong input setMatrixValue(10, 1, 5, ${path})`, () => {
			expect(() =>
				setMatrixValue(10, 1, 5, path)
			).toThrowErrorMatchingSnapshot()
		})
		test(`Testing wrong input setMatrixValue(1, 10, 5, ${path})`, () => {
			expect(() =>
				setMatrixValue(1, 10, 5, path)
			).toThrowErrorMatchingSnapshot()
		})
		test(`Testing wrong input setMatrixValue('a', 1, 5, ${path})`, () => {
			expect(() =>
				setMatrixValue('a', 1, 5, path)
			).toThrowErrorMatchingSnapshot()
		})
		test(`Testing wrong input setMatrixValue(true, 1, 5, ${path})`, () => {
			expect(() =>
				setMatrixValue(true, 1, 5, path)
			).toThrowErrorMatchingSnapshot()
		})
		test("Testing wrong input setMatrixValue(0, '1', 5, path)", () => {
			expect(() =>
				setMatrixValue(0, '1', 5, path)
			).toThrowErrorMatchingSnapshot()
		})
		test(`Testing wrong input setMatrixValue(0, true, 5, ${path})`, () => {
			expect(() =>
				setMatrixValue(0, true, 5, path)
			).toThrowErrorMatchingSnapshot()
		})
		test("Testing wrong input setMatrixValue(0, 1, '5', path)", () => {
			expect(() =>
				setMatrixValue(0, 1, '5', path)
			).toThrowErrorMatchingSnapshot()
		})
		test(`Testing wrong input setMatrixValue(0, 1, true, ${path})`, () => {
			expect(() =>
				setMatrixValue(0, 1, true, path)
			).toThrowErrorMatchingSnapshot()
		})
		test('Testing wrong input setMatrixValue(0, 1, 5, 5)', () => {
			expect(() => setMatrixValue(0, 1, 5, 5)).toThrowErrorMatchingSnapshot()
		})
		test('Testing wrong input setMatrixValue(0, 1, 5, true)', () => {
			expect(() => setMatrixValue(0, 1, 5, true)).toThrowErrorMatchingSnapshot()
		})
		test('Testing wrong input setMatrixValue(0, 1, 5)', () => {
			expect(() => setMatrixValue(0, 1, 5)).toThrowErrorMatchingSnapshot()
		})
	})

	describe('sumMatrix()', () => {
		test(`Testing correct input sumMatrix(2, 2, ${path})`, () => {
			expect(sumMatrix(2, 2, path)).toMatchSnapshot()
		})
		test(`Testing wrong input setMatrixValue(10, 1, 5, ${path})`, () => {
			expect(() => sumMatrix(10, 1, path)).toThrowErrorMatchingSnapshot()
		})
		test(`Testing wrong input setMatrixValue(1, 10, 5, ${path})`, () => {
			expect(() => sumMatrix(1, 10, path)).toThrowErrorMatchingSnapshot()
		})
		test(`Testing wrong input sumMatrix('a', 2, ${path})`, () => {
			expect(() => sumMatrix('a', 2, path)).toThrowErrorMatchingSnapshot()
		})
		test(`Testing wrong input sumMatrix(true, 2, ${path})`, () => {
			expect(() => sumMatrix(true, 2, path)).toThrowErrorMatchingSnapshot()
		})
		test("Testing wrong input sumMatrix(2, '2', path)", () => {
			expect(() => sumMatrix(2, '2', path)).toThrowErrorMatchingSnapshot()
		})
		test(`Testing wrong input sumMatrix(2, true, ${path})`, () => {
			expect(() => sumMatrix(2, true, path)).toThrowErrorMatchingSnapshot()
		})
		test('Testing wrong input sumMatrix(2, 2, 5)', () => {
			expect(() => sumMatrix(2, 2, 5)).toThrowErrorMatchingSnapshot()
		})
		test('Testing wrong input sumMatrix(2, 2, true)', () => {
			expect(() => sumMatrix(2, 2, true)).toThrowErrorMatchingSnapshot()
		})
	})
})
