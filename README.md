# NetSuite-test

[![pipeline status](https://gitlab.com/lukas.frantal/netsuite-test/badges/master/pipeline.svg)](https://gitlab.com/lukas.frantal/netsuite-test/-/commits/master)
[![coverage report](https://gitlab.com/lukas.frantal/netsuite-test/badges/master/coverage.svg)](https://gitlab.com/lukas.frantal/netsuite-test/-/commits/master)

if you want to run it you have to install [node.js](https://nodejs.org/en/)

if you want to run tasks you can run it by command:

```
npm run examples
```

if you want to run change something and create js files from typescript you can run build by command:

```
npm run build
```

if you want to run tests you can run it by command:

```
npm run test
```
